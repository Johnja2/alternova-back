package com.alternova.movie.query.api.dto;

import com.alternova.movie.common.dto.BaseResponse;

import com.alternova.movie.query.domain.Movie;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class MovieLookupResponseSingle extends BaseResponse {
    private Movie movie;
    public MovieLookupResponseSingle(String message){
        super(message);
    }
}