package com.alternova.movie.query.api.queries;

import com.alternova.cqrs.core.domain.BaseEntity;

import java.util.List;

public interface IMovieQueryHandler {
    List<BaseEntity> handle(FindAllMoviesQuery query);
    BaseEntity handle(FindMovieByIdQuery query);
    BaseEntity handle(FindAleatoryMovieQuery query);
    List<BaseEntity> handle(FindMovieByTypeQuery query);
    List<BaseEntity> handle(FindMovieByGenderQuery query);
    List<BaseEntity> handle(FindMovieByNameQuery query);
}
