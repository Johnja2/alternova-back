package com.alternova.movie.query.infrastructure.consumers;

import com.alternova.movie.common.events.MovieSavedEvent;
import com.alternova.movie.query.infrastructure.handlers.EventMovieHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public class MovieEventConsumer implements EventMovieConsumer {

    @Autowired
    private EventMovieHandler eventHandler;

    @KafkaListener(topics = "MovieSavedEvent", groupId = "${spring.kafka.consumer.group-id}")
    @Override
    public void consume(MovieSavedEvent event, Acknowledgment ack) {
            eventHandler.on(event);
            ack.acknowledge();
    }
}
