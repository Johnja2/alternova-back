package com.alternova.movie.query.api.dto;

import com.alternova.movie.common.dto.BaseResponse;
import com.alternova.movie.query.domain.Movie;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class MovieLookupResponseList extends BaseResponse {
    private List<Movie> movies;
    public MovieLookupResponseList(String message){
        super(message);
    }
}
