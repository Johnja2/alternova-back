package com.alternova.movie.query.infrastructure.consumers;

import com.alternova.movie.common.events.UserSavedEvent;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Payload;

public interface EventUserConsumer {
    void consume(@Payload UserSavedEvent event, Acknowledgment ack);
}
