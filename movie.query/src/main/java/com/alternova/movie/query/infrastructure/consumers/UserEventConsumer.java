package com.alternova.movie.query.infrastructure.consumers;

import com.alternova.movie.common.events.UserSavedEvent;
import com.alternova.movie.query.infrastructure.handlers.EventMovieHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public class UserEventConsumer implements EventUserConsumer {

    @Autowired
    private EventMovieHandler eventHandler;

    @KafkaListener(topics = "UserSavedEvent", groupId = "${spring.kafka.consumer.group-id}")
    @Override
    public void consume(UserSavedEvent event, Acknowledgment ack) {

    }
}
