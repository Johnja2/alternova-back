package com.alternova.movie.query.domain;

import com.alternova.cqrs.core.domain.BaseEntity;
import com.alternova.movie.common.dto.MovieType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {

    List<BaseEntity> findMoviesByMovie(String id);
}
