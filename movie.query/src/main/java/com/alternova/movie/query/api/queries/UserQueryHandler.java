package com.alternova.movie.query.api.queries;

import com.alternova.cqrs.core.domain.BaseEntity;
import com.alternova.movie.query.domain.Movie;
import com.alternova.movie.query.domain.User;
import com.alternova.movie.query.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class UserQueryHandler implements IUserQueryHandler{

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<BaseEntity> handle(FindAllUsersQuery query) {
        Iterable<User> movies = userRepository.findAll();
        List<BaseEntity> userList = new ArrayList<>();
        movies.forEach(userList::add);
        return userList;
    }

    @Override
    public BaseEntity handle(FindUserByIdQuery query) {
        var user = userRepository.findById(query.getId());
        if(user.isEmpty()){
            return null;
        }
        return user.get();
    }

    @Override
    public List<BaseEntity> handle(FindMoviesByUserQuery query) {
        var movies = userRepository.findMoviesByMovie(query.getId());
        if(movies.isEmpty()){
            return null;
        }
        return movies;
    }
}
