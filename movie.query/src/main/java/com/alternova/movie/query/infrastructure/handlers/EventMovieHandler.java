package com.alternova.movie.query.infrastructure.handlers;

import com.alternova.movie.common.events.MovieSavedEvent;

public interface EventMovieHandler {
    void on(MovieSavedEvent event);
}
