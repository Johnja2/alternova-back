package com.alternova.movie.query.api.controllers;

import com.alternova.cqrs.core.infrastructure.QueryDispatcher;

import com.alternova.movie.query.api.dto.MovieLookupResponseList;
import com.alternova.movie.query.api.dto.UserLookupResponseList;
import com.alternova.movie.query.api.dto.UserLookupResponseSingle;
import com.alternova.movie.query.api.queries.FindAllUsersQuery;
import com.alternova.movie.query.api.queries.FindMoviesByUserQuery;
import com.alternova.movie.query.api.queries.FindUserByIdQuery;
import com.alternova.movie.query.domain.Movie;
import com.alternova.movie.query.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(path = "/api/v1/userLookup")
public class UserLookupController {

    private final Logger logger = Logger.getLogger(UserLookupController.class.getName());

    @Autowired
    private QueryDispatcher queryDispatcher;

    @GetMapping(path = "/")
    public ResponseEntity<UserLookupResponseList> getAllUsers(){
        try{
            List<User> users = queryDispatcher.send(new FindAllUsersQuery());
            if(users == null || users.size() == 0){
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            var response = UserLookupResponseList.builder()
                    .users(users)
                    .message(MessageFormat.format("Successfully", null))
                    .build();

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            var safeErrorMessage = "Error";
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new UserLookupResponseList(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<UserLookupResponseSingle> getUserById(@PathVariable(value="id") String id){
        try{
            List<User> users = queryDispatcher.send(new FindUserByIdQuery());
            if(users == null || users.size() == 0){
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            var response = UserLookupResponseSingle.builder()
                    .user(users.get(0))
                    .message(MessageFormat.format("Successfully", null))
                    .build();

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            var safeErrorMessage = "Error";
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new UserLookupResponseSingle(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/userMovies/{id}")
    public ResponseEntity<MovieLookupResponseList> getMoviesByUser(@PathVariable(value="id") String id){
        try{
            List<Movie> movies = queryDispatcher.send(new FindMoviesByUserQuery());
            if(movies == null || movies.size() == 0){
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            var response = MovieLookupResponseList.builder()
                    .movies(movies)
                    .message(MessageFormat.format("Successfully", null))
                    .build();

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            var safeErrorMessage = "Error";
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new MovieLookupResponseList(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
