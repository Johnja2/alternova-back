package com.alternova.movie.query.infrastructure;

import com.alternova.cqrs.core.domain.BaseEntity;
import com.alternova.cqrs.core.infrastructure.QueryDispatcher;
import com.alternova.cqrs.core.queries.BaseQuery;
import com.alternova.cqrs.core.queries.QueryHandlerMethod;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class UserQueryDispatcher implements QueryDispatcher {
    private final Map<Class<? extends BaseQuery>, List<QueryHandlerMethod>> routes = new HashMap<>();

    @Override
    public <T extends BaseQuery> void registerHandler(Class<T> type, QueryHandlerMethod<T> handler) {
        var handlers = routes.computeIfAbsent(type, c -> new LinkedList<>());
        handlers.add(handler);
    }

    @Override
    public <U extends BaseEntity> List<U> send(BaseQuery query) {
        var handlers = routes.get(query.getClass());
        if(handlers == null || handlers.size() == 0) {
            throw new RuntimeException("Any query handler were register");
        }
        if(handlers.size() > 1){
            throw  new RuntimeException("One query hasn't more the one handler");
        }

        return handlers.get(0).handle(query);
    }
}
