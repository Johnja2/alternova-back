package com.alternova.movie.query.domain;

import com.alternova.cqrs.core.domain.BaseEntity;
import com.alternova.movie.common.dto.DocumentType;
import com.alternova.movie.common.events.MovieSavedEvent;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(uniqueConstraints =
        {
                @UniqueConstraint(name = "UniqueDocumentTypeAndDocumentId", columnNames = { "documentType", "documentId" })})
public class User extends BaseEntity {
    @Id
    private String id;
    private String fullName;
    private DocumentType documentType;
    private String documentId;
    @Column(unique=true)
    private String email;
    @ManyToOne
    @JoinColumn
    private Movie movie;
}
