package com.alternova.movie.query.api.queries;

import com.alternova.cqrs.core.domain.BaseEntity;
import com.alternova.movie.query.domain.Movie;
import com.alternova.movie.query.domain.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovieQueryHandler implements IMovieQueryHandler {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public List<BaseEntity> handle(FindAllMoviesQuery query) {
        Iterable<Movie> movies = movieRepository.findAll();
        List<BaseEntity> movieList = new ArrayList<>();
        movies.forEach(movieList::add);
        return movieList;
    }

    @Override
    public BaseEntity handle(FindMovieByIdQuery query) {
        var movie = movieRepository.findById(query.getId());
        if(movie.isEmpty()){
            return null;
        }
        return movie.get();
    }

    @Override
    public BaseEntity handle(FindAleatoryMovieQuery query) {
        Iterable<Movie> movies = movieRepository.findAll();
        List<BaseEntity> movieList = new ArrayList<>();
        movies.forEach(movieList::add);
        int randomNumber = (int) (Math.random() * movieList.size());
        return movieList.get(randomNumber);
    }

    @Override
    public List<BaseEntity> handle(FindMovieByTypeQuery query) {
        var movies = movieRepository.findByMovieType(query.getMovieType());
        if(movies.isEmpty()){
            return null;
        }
        return movies;
    }

    @Override
    public List<BaseEntity> handle(FindMovieByGenderQuery query) {
        var movies = movieRepository.findByMovieGender(query.getMovieGender());
        if(movies.isEmpty()){
            return null;
        }
        return movies;
    }

    @Override
    public List<BaseEntity> handle(FindMovieByNameQuery query) {
        var movies = movieRepository.findByMovieName(query.getFullName());
        if(movies.isEmpty()){
            return null;
        }
        List<BaseEntity> movieList = new ArrayList<>();
        movieList.add(movies.get());
        return movieList;
    }
}
