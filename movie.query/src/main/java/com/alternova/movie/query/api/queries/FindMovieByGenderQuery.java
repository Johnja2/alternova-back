package com.alternova.movie.query.api.queries;

import com.alternova.cqrs.core.queries.BaseQuery;
import com.alternova.movie.common.dto.MovieGender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FindMovieByGenderQuery extends BaseQuery {
    private MovieGender movieGender;
}
