package com.alternova.movie.query.infrastructure.consumers;

import com.alternova.movie.common.events.MovieSavedEvent;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Payload;

public interface EventMovieConsumer {
    void consume(@Payload MovieSavedEvent event, Acknowledgment ack);
}
