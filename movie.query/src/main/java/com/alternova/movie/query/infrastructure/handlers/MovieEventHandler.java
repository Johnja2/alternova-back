package com.alternova.movie.query.infrastructure.handlers;

import com.alternova.movie.common.events.MovieSavedEvent;
import com.alternova.movie.query.domain.Movie;
import com.alternova.movie.query.domain.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class MovieEventHandler implements EventMovieHandler {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public void on(MovieSavedEvent event) {
        var movie = Movie.builder()
                .id(event.getId())
                .build();
        movieRepository.save(movie);
    }
}
