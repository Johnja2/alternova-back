package com.alternova.movie.query;

import com.alternova.cqrs.core.infrastructure.QueryDispatcher;
import com.alternova.movie.query.api.queries.FindAllMoviesQuery;
import com.alternova.movie.query.api.queries.IMovieQueryHandler;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class QueryApplication {

	@Autowired
	private QueryDispatcher queryDispatcher;

	@Autowired
	private IMovieQueryHandler queryHandler;

	public static void main(String[] args) {
		SpringApplication.run(QueryApplication.class, args);
	}

	@PostConstruct
	public void registerHandlers(){
		queryDispatcher.registerHandler(FindAllMoviesQuery.class, queryHandler::handle);
	}
}
