package com.alternova.movie.query.api.controllers;

import com.alternova.cqrs.core.infrastructure.QueryDispatcher;
import com.alternova.movie.query.api.dto.MovieLookupResponseList;
import com.alternova.movie.query.api.queries.FindAllMoviesQuery;
import com.alternova.movie.query.domain.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(path = "/api/v1/movieLookup")
public class MovieLookupController {
    private final Logger logger = Logger.getLogger(MovieLookupController.class.getName());

    @Autowired
    private QueryDispatcher queryDispatcher;

    @GetMapping
    public ResponseEntity<MovieLookupResponseList> getAllMovies(){
        try{
            List<Movie> movies = queryDispatcher.send(new FindAllMoviesQuery());
            if(movies == null || movies.size() == 0){
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }
            var response = MovieLookupResponseList.builder()
                    .movies(movies)
                    .message(MessageFormat.format("Successfully", null))
                    .build();

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e){
            var safeErrorMessage = "Error on the consult";
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new MovieLookupResponseList(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
