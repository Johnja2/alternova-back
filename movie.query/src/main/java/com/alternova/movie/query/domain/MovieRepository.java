package com.alternova.movie.query.domain;

import com.alternova.cqrs.core.domain.BaseEntity;
import com.alternova.movie.common.dto.MovieGender;
import com.alternova.movie.common.dto.MovieType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface MovieRepository extends CrudRepository<Movie, String> {
    Optional<Movie> findByMovieName(String name);
    List<BaseEntity> findByMovieType(MovieType movieType);
    List<BaseEntity> findByMovieGender(MovieGender movieGender);
}
