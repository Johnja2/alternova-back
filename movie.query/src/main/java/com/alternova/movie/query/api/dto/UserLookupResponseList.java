package com.alternova.movie.query.api.dto;

import com.alternova.movie.common.dto.BaseResponse;
import com.alternova.movie.query.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UserLookupResponseList extends BaseResponse {

    private List<User> users;

    public UserLookupResponseList(String message){
        super(message);
    }
}
