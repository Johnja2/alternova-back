package com.alternova.movie.query.api.queries;

import com.alternova.cqrs.core.domain.BaseEntity;

import java.util.List;

public interface IUserQueryHandler {
    List<BaseEntity> handle(FindAllUsersQuery query);
    BaseEntity handle(FindUserByIdQuery query);
    List<BaseEntity> handle(FindMoviesByUserQuery query);
}
