package com.alternova.movie.query.api.dto;

import com.alternova.movie.common.dto.BaseResponse;
import com.alternova.movie.query.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UserLookupResponseSingle extends BaseResponse {

    private User user;

    public UserLookupResponseSingle(String message){
        super(message);
    }
}
