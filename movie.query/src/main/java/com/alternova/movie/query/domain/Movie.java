package com.alternova.movie.query.domain;

import com.alternova.cqrs.core.domain.BaseEntity;
import com.alternova.movie.common.dto.MovieGender;
import com.alternova.movie.common.dto.MovieType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Movie extends BaseEntity {

    @Id
    private String id;
    private String name;
    private MovieGender movieGender;
    private MovieType movieType;
    private int viewNumber;
    private double score;
}
