package com.alternova.movie.query.api.queries;

import com.alternova.cqrs.core.queries.BaseQuery;
import com.alternova.movie.common.dto.MovieType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FindMovieByTypeQuery extends BaseQuery {
    private MovieType movieType;

}
