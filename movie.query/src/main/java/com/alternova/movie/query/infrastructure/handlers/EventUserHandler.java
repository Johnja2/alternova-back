package com.alternova.movie.query.infrastructure.handlers;

import com.alternova.movie.common.events.UserSavedEvent;

public interface EventUserHandler {

    void on(UserSavedEvent event);
}
