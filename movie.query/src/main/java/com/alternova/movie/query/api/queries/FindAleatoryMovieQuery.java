package com.alternova.movie.query.api.queries;

import com.alternova.cqrs.core.queries.BaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FindAleatoryMovieQuery extends BaseQuery {
}
