package com.alternova.movie.common.dto;

public enum MovieGender {
    ACTION,
    ANIMATION,
    BIOPIC,
    CARTOON,
    COMEDY,
    DRAMA,
    EDUCATIONAL,
    DOCUMENTARY,
    FANTASY,
    HORROR,
    ROMANTIC,
    SCARY,
    WAR,
    THRILLER
}
