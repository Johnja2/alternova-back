package com.alternova.movie.common.events;

import com.alternova.cqrs.core.events.BaseEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class MovieUpdatedEvent extends BaseEvent {
    private int viewNumber;
    private double score;
}
