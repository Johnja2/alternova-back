package com.alternova.movie.common.events;

import com.alternova.movie.common.dto.MovieGender;
import com.alternova.movie.common.dto.MovieType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class MovieSavedEvent extends BaseEvent {
    private String name;
    private MovieGender movieGender;
    private MovieType movieType;
    private int viewNumber;
    private double score;
}
