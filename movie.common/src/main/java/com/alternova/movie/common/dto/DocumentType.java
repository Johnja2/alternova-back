package com.alternova.movie.common.dto;

public enum DocumentType {
    CC,
    CE,
    NIT,
    PP
}
