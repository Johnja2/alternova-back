package com.alternova.movie.common.events;

import com.alternova.cqrs.core.events.BaseEvent;
import com.alternova.movie.common.dto.DocumentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class UserSavedEvent extends BaseEvent {
    private String fullName;
    private DocumentType documentType;
    private String documentId;
    private String email;
    private MovieSavedEvent movie;
}
