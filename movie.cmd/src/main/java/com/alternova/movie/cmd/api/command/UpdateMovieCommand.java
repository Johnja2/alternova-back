package com.alternova.movie.cmd.api.command;

import com.alternova.cqrs.core.commands.BaseCommand;
import lombok.Data;

@Data
public class UpdateMovieCommand extends BaseCommand {
    private int viewNumber;
    private double score;
}
