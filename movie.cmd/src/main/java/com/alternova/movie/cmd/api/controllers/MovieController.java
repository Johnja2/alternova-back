package com.alternova.movie.cmd.api.controllers;

import com.alternova.cqrs.core.infrastructure.CommandDispatcher;
import com.alternova.movie.cmd.api.command.SaveMovieCommand;
import com.alternova.movie.cmd.api.dto.SaveMovieResponse;
import com.alternova.movie.cmd.api.dto.UpdateMovieResponse;
import com.alternova.movie.common.dto.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.MessageFormat;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(path ="/api/v1/movie")
public class MovieController {

    private final Logger logger = Logger.getLogger(MovieController.class.getName());

    @Autowired
    private CommandDispatcher commandDispatcher;

    @PostMapping
    public ResponseEntity<BaseResponse> saveMovie(@RequestBody SaveMovieCommand command){
        var id = UUID.randomUUID().toString();
        command.setId(id);
        try {
            commandDispatcher.send(command);
            return new ResponseEntity<>(new SaveMovieResponse("The movie saved", id), HttpStatus.CREATED);

        } catch(IllegalStateException e){
            logger.log(Level.WARNING, MessageFormat.format("Don't save movie - {0}",e.toString()));
            return new ResponseEntity<>(new BaseResponse(e.toString()), HttpStatus.BAD_REQUEST);
        } catch(Exception e){
            var safeErrorMessage = MessageFormat.format("Error while to processing the save - {0}", id);
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new SaveMovieResponse(safeErrorMessage, id), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping
    public ResponseEntity<BaseResponse> updateMovie(@RequestBody SaveMovieCommand command, @RequestParam String id){
        try {
            commandDispatcher.send(command);
            return new ResponseEntity<>(new UpdateMovieResponse("The movie is updated", id), HttpStatus.CREATED);
        } catch(IllegalStateException e){
            logger.log(Level.WARNING, MessageFormat.format("Don't update - {0}",e.toString()));
            return new ResponseEntity<>(new BaseResponse(e.toString()), HttpStatus.BAD_REQUEST);
        } catch(Exception e){
            var safeErrorMessage = MessageFormat.format("Error while to processing the update - {0}", id);
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new SaveMovieResponse(safeErrorMessage, id), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
