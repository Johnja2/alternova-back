package com.alternova.movie.cmd.api.dto;

import com.alternova.movie.common.dto.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateMovieResponse extends BaseResponse {

    private String id;

    public UpdateMovieResponse(String message, String  id){
        super(message);
        this.id = id;
    }
}
