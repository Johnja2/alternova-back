package com.alternova.movie.cmd.api.command;

public interface CommandHandler {
    void handle(SaveMovieCommand command);
    void handle(UpdateMovieCommand command);
}
