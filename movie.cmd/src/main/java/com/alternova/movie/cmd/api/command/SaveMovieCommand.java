package com.alternova.movie.cmd.api.command;

import com.alternova.cqrs.core.commands.BaseCommand;
import com.alternova.movie.common.dto.MovieGender;
import com.alternova.movie.common.dto.MovieType;
import lombok.Data;

@Data
public class SaveMovieCommand extends BaseCommand {
    private String name;
    private MovieGender movieGender;
    private MovieType movieType;
    private int viewNumber;
    private double score;
}
