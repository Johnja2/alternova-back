package com.alternova.movie.cmd;

import com.alternova.cqrs.core.infrastructure.CommandDispatcher;
import com.alternova.movie.cmd.api.command.CommandHandler;
import com.alternova.movie.cmd.api.command.SaveMovieCommand;
import com.alternova.movie.cmd.api.command.UpdateMovieCommand;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class CommandApplication {

	@Autowired
	private CommandDispatcher commandDispatcher;

	@Autowired
	private CommandHandler commandHandler;

	public static void main(String[] args) {
		SpringApplication.run(CommandApplication.class, args);
	}

	@PostConstruct
	public void registerHandlers(){
		commandDispatcher.registerHandler(SaveMovieCommand.class, commandHandler::handle);
		commandDispatcher.registerHandler(UpdateMovieCommand.class, commandHandler::handle);
	}

}
