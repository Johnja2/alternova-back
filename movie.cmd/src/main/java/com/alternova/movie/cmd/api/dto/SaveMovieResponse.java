package com.alternova.movie.cmd.api.dto;

import com.alternova.movie.common.dto.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SaveMovieResponse extends BaseResponse {

    private String id;

    public SaveMovieResponse(String message, String  id){
        super(message);
        this.id = id;
    }
}
