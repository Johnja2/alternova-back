package com.alternova.movie.cmd.api.command;

import com.alternova.cqrs.core.handlers.EventSourcingHandler;
import com.alternova.movie.cmd.domain.MovieAggregate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieCommandHandler implements CommandHandler{

    @Autowired
    private EventSourcingHandler<MovieAggregate> eventSourcingHandler;

    @Override
    public void handle(SaveMovieCommand command) {
        var aggregate = new MovieAggregate(command);
        eventSourcingHandler.save(aggregate);
    }

    @Override
    public void handle(UpdateMovieCommand command) {
        var aggregate = eventSourcingHandler.getById(command.getId());
        aggregate.updateMovie(command);
        eventSourcingHandler.save(aggregate);
    }
}
