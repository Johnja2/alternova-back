package com.alternova.movie.cmd.domain;

import com.alternova.cqrs.core.domain.AggregateRoot;
import com.alternova.movie.cmd.api.command.SaveMovieCommand;
import com.alternova.movie.cmd.api.command.UpdateMovieCommand;
import com.alternova.movie.common.events.MovieSavedEvent;
import com.alternova.movie.common.events.MovieUpdatedEvent;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MovieAggregate extends AggregateRoot {
    private double score;
    private int viewNumber;

    public MovieAggregate(SaveMovieCommand command){
        raiseEvent(MovieSavedEvent.builder()
                .id(command.getId())
                .name(command.getName())
                .movieGender(command.getMovieGender())
                .movieType(command.getMovieType())
                .viewNumber(command.getViewNumber())
                .score(command.getScore())
                .build()
        );
    }

    public void apply(MovieSavedEvent event){
        this.id = event.getId();
    }

    public void updateMovie(UpdateMovieCommand command){
        raiseEvent(MovieUpdatedEvent.builder()
                .id(this.id)
                .viewNumber(command.getViewNumber())
                .score(command.getScore())
                .build());
    }

    public void apply(MovieUpdatedEvent event){
        this.id = event.getId();
        this.viewNumber = event.getViewNumber();
        this.score = event.getScore();
    }
}
