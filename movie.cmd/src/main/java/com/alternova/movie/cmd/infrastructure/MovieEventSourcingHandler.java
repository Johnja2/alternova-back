package com.alternova.movie.cmd.infrastructure;

import com.alternova.cqrs.core.domain.AggregateRoot;
import com.alternova.cqrs.core.handlers.EventSourcingHandler;
import com.alternova.cqrs.core.infrastructure.EventStore;
import com.alternova.movie.cmd.domain.MovieAggregate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;

@Service
public class MovieEventSourcingHandler implements EventSourcingHandler<MovieAggregate> {
    @Autowired
    private EventStore eventStore;

    @Override
    public void save(AggregateRoot aggregate) {
        eventStore.saveEvents(aggregate.getId(), aggregate.getUncommittedChanges(), aggregate.getVersion());
        aggregate.markChangesAsCommitted();
    }

    @Override
    public MovieAggregate getById(String id) {
        var aggregate = new MovieAggregate();
        var events = eventStore.getEvent(id);
        if(events != null && !events.isEmpty()){
            aggregate.replayEvent(events);
            var latestVersion = events.stream().map(x -> x.getVersion()).max(Comparator.naturalOrder());
            aggregate.setVersion(latestVersion.get());
        }
        return aggregate;
    }
}
