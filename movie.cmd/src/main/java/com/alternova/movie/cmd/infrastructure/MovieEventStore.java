package com.alternova.movie.cmd.infrastructure;

import com.alternova.cqrs.core.events.BaseEvent;
import com.alternova.cqrs.core.events.EventModel;
import com.alternova.cqrs.core.exceptions.AggregateNotFoundException;
import com.alternova.cqrs.core.exceptions.ConcurrencyException;
import com.alternova.cqrs.core.infrastructure.EventStore;
import com.alternova.cqrs.core.producers.EventProducer;
import com.alternova.movie.cmd.domain.EventStoreRepository;
import com.alternova.movie.cmd.domain.MovieAggregate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieEventStore implements EventStore {

    @Autowired
    private EventStoreRepository eventStoreRepository;

    @Autowired
    private EventProducer eventProducer;

    @Override
    public void saveEvents(String aggregateId, Iterable<BaseEvent> events, int expectedVersion) {
        var eventStream = eventStoreRepository.findByAggregateIdentifier(aggregateId);
        if(expectedVersion != -1 && eventStream.get(eventStream.size() - 1).getVersion() != expectedVersion){
            throw new ConcurrencyException("Concurrency exception");
        }

        var version = expectedVersion;
        for(var event: events){
            version++;
            event.setVersion(version);
            var eventModel = EventModel.builder()
                    .timeStamp(new Date())
                    .aggregateIdentifier(aggregateId)
                    .aggregateType(MovieAggregate.class.getTypeName())
                    .eventType(event.getClass().getTypeName())
                    .eventData(event)
                    .build();
            var persistedEvent= eventStoreRepository.save(eventModel);
            if(!persistedEvent.getId().isEmpty()){
                eventProducer.producer(event.getClass().getSimpleName(), event);
            }
        }
    }

    @Override
    public List<BaseEvent> getEvent(String aggregateId) {
        var eventStream = eventStoreRepository.findByAggregateIdentifier(aggregateId);
        if(eventStream == null || eventStream.isEmpty()){
            throw new AggregateNotFoundException("The movie Id is incorrect");
        }
        return eventStream.stream().map(x -> x.getEventData()).collect(Collectors.toList());
    }
}
