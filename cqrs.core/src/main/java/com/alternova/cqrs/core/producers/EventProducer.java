package com.alternova.cqrs.core.producers;

import com.alternova.cqrs.core.events.BaseEvent;

public interface EventProducer {
    void producer(String topic, BaseEvent event);
}
