package com.alternova.cqrs.core.users;

import com.alternova.cqrs.core.domain.BaseEntity;

import java.util.List;

@FunctionalInterface
public interface UserHandlerMethod<T extends BaseUser> {
    List<BaseEntity> handle(T user);
}
