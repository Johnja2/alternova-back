package com.alternova.cqrs.core.exceptions;

public class ConcurrencyException extends RuntimeException {
    public ConcurrencyException(String message){
        super(message);
    }
}
