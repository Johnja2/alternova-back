package com.alternova.cqrs.core.infrastructure;

import com.alternova.cqrs.core.domain.BaseEntity;
import com.alternova.cqrs.core.queries.BaseQuery;
import com.alternova.cqrs.core.queries.QueryHandlerMethod;

import java.util.List;

public interface QueryDispatcher {
    <T extends BaseQuery> void registerHandler(Class<T> type, QueryHandlerMethod<T> handler);
    <U extends BaseEntity> List<U> send(BaseQuery query);
 }
