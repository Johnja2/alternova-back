package com.alternova.cqrs.core.infrastructure;

import com.alternova.cqrs.core.domain.BaseEntity;
import com.alternova.cqrs.core.users.BaseUser;
import com.alternova.cqrs.core.users.UserHandlerMethod;

import java.util.List;

public interface UserDispatcher {
    <T extends BaseUser> void registerHandler(Class<T> type, UserHandlerMethod<T> handler);

    <U extends BaseEntity> List<U> send(BaseUser user);
}
