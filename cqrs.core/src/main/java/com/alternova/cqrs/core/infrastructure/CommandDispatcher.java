package com.alternova.cqrs.core.infrastructure;

import com.alternova.cqrs.core.commands.BaseCommand;
import com.alternova.cqrs.core.commands.CommandHandlerMethod;

public interface CommandDispatcher {
    <T extends BaseCommand> void registerHandler(Class<T> type, CommandHandlerMethod<T> handler);
    void send(BaseCommand command);
}
