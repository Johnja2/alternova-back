package com.alternova.movie.user.domain;

import com.alternova.cqrs.core.domain.AggregateRoot;
import com.alternova.movie.common.events.MovieSavedEvent;
import com.alternova.movie.common.events.MovieUpdatedEvent;
import com.alternova.movie.common.events.UserSavedEvent;
import com.alternova.movie.common.events.UserUpdatedEvent;
import com.alternova.movie.user.api.command.SaveUserCommand;
import com.alternova.movie.user.api.command.UpdateUserCommand;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAggregate extends AggregateRoot {

    private MovieSavedEvent movieSavedEvent;

    public UserAggregate(SaveUserCommand command){
        raiseEvent(UserSavedEvent.builder()
                .id(command.getId())
                .fullName(command.getFullName())
                .documentType(command.getDocumentType())
                .documentId(command.getDocumentId())
                .email(command.getEmail())
                .build()
        );
    }

    public void apply(UserSavedEvent event){
        this.id = event.getId();
    }

    public void updateUser(UpdateUserCommand command){
        raiseEvent(UserUpdatedEvent.builder()
                .id(this.id)
                .movieSavedEvent(command.getMovieSavedEvent())
                .build());
    }

    public void apply(UserUpdatedEvent event){
        this.id = event.getId();
        this.movieSavedEvent = event.getMovieSavedEvent();
    }
}
