package com.alternova.movie.user.api.command;

import com.alternova.cqrs.core.commands.BaseCommand;
import com.alternova.movie.common.events.MovieSavedEvent;
import lombok.Data;

@Data
public class UpdateUserCommand extends BaseCommand {
    private MovieSavedEvent movieSavedEvent;
}
