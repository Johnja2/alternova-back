package com.alternova.movie.user.api.command;

import com.alternova.cqrs.core.commands.BaseCommand;
import com.alternova.movie.common.dto.DocumentType;
import lombok.Data;

@Data
public class SaveUserCommand extends BaseCommand {
    private String fullName;
    private DocumentType documentType;
    private String documentId;
    private String email;
}
