package com.alternova.movie.user.api.controllers;

import com.alternova.cqrs.core.infrastructure.CommandDispatcher;
import com.alternova.movie.common.dto.BaseResponse;
import com.alternova.movie.user.api.dto.SaveUserResponse;
import com.alternova.movie.user.api.command.SaveUserCommand;
import com.alternova.movie.user.api.dto.UpdateUserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.MessageFormat;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(path ="/api/v1/user")
public class UserController {

    private final Logger logger = Logger.getLogger(UserController.class.getName());

    @Autowired
    private CommandDispatcher commandDispatcher;

    @PostMapping
    public ResponseEntity<BaseResponse> saveUser(@RequestBody SaveUserCommand command){
        var id = UUID.randomUUID().toString();
        command.setId(id);
        try {
            commandDispatcher.send(command);
            return new ResponseEntity<>(new SaveUserResponse("The user is saved", id), HttpStatus.CREATED);

        } catch(IllegalStateException e){
            logger.log(Level.WARNING, MessageFormat.format("Don't save - {0}",e.toString()));
            return new ResponseEntity<>(new BaseResponse(e.toString()), HttpStatus.BAD_REQUEST);
        } catch(Exception e){
            var safeErrorMessage = MessageFormat.format("Error while processing the request - {0}", id);
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new SaveUserResponse(safeErrorMessage, id), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping()
    public ResponseEntity<BaseResponse> updateUser(@RequestBody SaveUserCommand command, @RequestParam String id){
        try {
            commandDispatcher.send(command);
            return new ResponseEntity<>(new UpdateUserResponse("The user is updated as movie aggregate", id), HttpStatus.CREATED);
        } catch(IllegalStateException e){
            logger.log(Level.WARNING, MessageFormat.format("Don't update - {0}",e.toString()));
            return new ResponseEntity<>(new BaseResponse(e.toString()), HttpStatus.BAD_REQUEST);
        } catch(Exception e){
            var safeErrorMessage = MessageFormat.format("Error while to processing the update - {0}", id);
            logger.log(Level.SEVERE, safeErrorMessage, e);
            return new ResponseEntity<>(new SaveUserResponse(safeErrorMessage, id), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
