package com.alternova.movie.user.infrastructure;

import com.alternova.cqrs.core.domain.AggregateRoot;
import com.alternova.cqrs.core.handlers.EventSourcingHandler;
import com.alternova.cqrs.core.infrastructure.EventStore;
import com.alternova.movie.user.domain.UserAggregate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;

@Service
public class UserEventSourcingHandler implements EventSourcingHandler<UserAggregate> {
    @Autowired
    private EventStore eventStore;

    @Override
    public void save(AggregateRoot aggregate) {
        eventStore.saveEvents(aggregate.getId(), aggregate.getUncommittedChanges(), aggregate.getVersion());
        aggregate.markChangesAsCommitted();
    }

    @Override
    public UserAggregate getById(String id) {
        var aggregate = new UserAggregate();
        var events = eventStore.getEvent(id);
        if(events != null && !events.isEmpty()){
            aggregate.replayEvent(events);
            var latestVersion = events.stream().map(x -> x.getVersion()).max(Comparator.naturalOrder());
            aggregate.setVersion(latestVersion.get());
        }
        return aggregate;
    }
}
