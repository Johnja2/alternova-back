package com.alternova.movie.user.api.command;

public interface CommandHandler {
    void handle(SaveUserCommand command);

    void handle(UpdateUserCommand command);
}
