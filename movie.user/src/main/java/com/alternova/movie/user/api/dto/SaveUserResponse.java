package com.alternova.movie.user.api.dto;

import com.alternova.movie.common.dto.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SaveUserResponse extends BaseResponse {
    private String id;
    public SaveUserResponse(String message, String  id){
        super(message);
        this.id = id;
    }
}
