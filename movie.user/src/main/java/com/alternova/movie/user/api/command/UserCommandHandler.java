package com.alternova.movie.user.api.command;

import com.alternova.cqrs.core.handlers.EventSourcingHandler;
import com.alternova.movie.user.domain.UserAggregate;
import org.springframework.beans.factory.annotation.Autowired;

public class UserCommandHandler implements CommandHandler {

    @Autowired
    private EventSourcingHandler<UserAggregate> eventSourcingHandler;

    @Override
    public void handle(SaveUserCommand command) {
        var aggregate = new UserAggregate(command);
        eventSourcingHandler.save(aggregate);
    }

    @Override
    public void handle(UpdateUserCommand command) {
        var aggregate = eventSourcingHandler.getById(command.getId());
        aggregate.updateUser(command);
        eventSourcingHandler.save(aggregate);
    }
}
