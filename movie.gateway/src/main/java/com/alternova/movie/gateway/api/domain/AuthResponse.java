package com.alternova.movie.gateway.api.domain;

import com.alternova.movie.common.dto.BaseResponse;

import java.util.List;

public class AuthResponse extends BaseResponse {
    private Jwt jwtToken;

    public AuthResponse(String message){
        super(message);
    }
}
