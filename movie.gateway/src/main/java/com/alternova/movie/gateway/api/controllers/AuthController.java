package com.alternova.movie.gateway.api.controllers;

import com.alternova.movie.gateway.domain.JwtToken;
import com.alternova.movie.gateway.domain.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {

  @Autowired
  private JwtToken jwtToken;

    @PostMapping("/login")
    public ResponseEntity<> login(@RequestBody Login login){
        Authentication authentication =
                authenticationManager.authenticate(new UserPasswordAuthenticationToken(loginDTO.getUser(), loginDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateToken(authentication);
        Jwt jwtDto = new Jwt(jwt, userDetails.getUsername(), userDetails.getAuthorities());
        return new ResponseEntity(jwtDto, HttpStatus.OK);
    }
}
